# Configuration Options
Auto generated from .spec files

## Top Level

Option: `commands` - Command Map Information
 - Type: dict
 - Required: True
 - Default: {}
 - Example: {'a_command': {}, 'some_command': {}}
 - Additionally Validates With: `argmap.commands`

Option: `auth_args` - Arguments required when `auth_required=True`
 - Type: dict
 - Required: False
 - Default: {}
 - Additionally Validates With: `argmap.command.args-root`

## Spec for argmap.command.args-root

Option: `__any_item__` - Data Definition
 - Type: any
 - Required: False
 - Example: {'myarg': {'type': 'str', 'cli_flag_names': ['-M', '--my-arg'], 'required': True, 'help': 'My Argument'}, 'otherarg': {'type': 'str', 'cli_flag_names': ['-z', '--other-arg'], 'required': True, 'help': 'My Other Argument'}}
 - Additionally Validates With: `argmap.command.args`

## Spec for argmap.command

Option: `auth_required` - Whether or not Command requires to be authorized
 - Type: bool
 - Required: True
 - Default: True

Option: `ui_label` - UI Label
 - Type: str
 - Required: True
 - Default:

Option: `help` - Command Help
 - Type: str
 - Required: True

Option: `cli_hidden` - Hide from CLI
 - Type: bool
 - Required: False
 - Default: False

Option: `api_hidden` - Hide from API
 - Type: bool
 - Required: False
 - Default: True

Option: `args` - Command Argument configurations
 - Type: dict
 - Required: False
 - Default: {}
 - Example: {'myarg': {}, 'otherarg': {}}
 - Additionally Validates With: `argmap.command.args-root`

Option: `group` - Command 'group' name for organization
 - Type: str
 - Required: False
 - Default:

## Spec for argmap.command.args

Option: `type` - Argument value type
 - Type: str
 - Required: True
 - Acceptable Values: any, str, int, list, dict, float, bool

Option: `required` - Whether or not Argument is required
 - Type: bool
 - Required: True
 - Default: False

Option: `default` - Default Argument Value
 - Type: any
 - Required: False

Option: `help` - Argument Help Text
 - Type: str
 - Required: True

Option: `cli_flag_names` - Commandline Flag Name(s)
 - Type: list
 - Required: True
 - Example: ['--my-arg', '-M']

Option: `multi` - Whether or not to set `nargs='+'`
 - Type: bool
 - Required: False
 - Default: False

Option: `any_arg` - Used when multi=true, allows for `--<arg_name> k=v` and breaking `args[arg_name] = [ 'k=v' ]` to dict
 - Type: bool
 - Required: False
 - Default: False

Option: `values` - Allowable values for argument
 - Type: list
 - Required: False
 - Example: ['a', 'foo', 'bar']

## Spec for argmap.commands

Option: `__any_item__` - Command Data
 - Type: dict
 - Required: False
 - Example: {'auth_required': True, 'api_hidden': False}
 - Additionally Validates With: `argmap.command`
