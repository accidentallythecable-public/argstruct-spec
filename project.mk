
PROJECT_OWNER := AccidentallyTheCable
PROJECT_EMAIL := cableninja@cableninja.net
PROJECT_FIRST_YEAR := 2023
PROJECT_LICENSE := GPLv3
PROJECT_NAME := argstruct-specification
PROJECT_DESCRIPTION := The ArgStruct Specification
PROJECT_VERSION := 1.1.0

## Enable Feature 'Python'
BUILD_PYTHON := 0
## Enable Feature 'Shell'
BUILD_SHELL := 0
## Enable Feature 'Docker'
BUILD_DOCKER := 0
## Enable python `dist` Phase for Projects destined for PYPI
PYTHON_PYPI_PROJECT := 0
## Additional Flags for pylint. EX --ignore-paths=mypath
PYLINT_EXTRA_FLAGS := 

### Any Further Project-specific make targets can go here

spec_project_version:  ## Fix Version information in this project
	sed -ri "s/(.*)VERSION: ((([0-9]{1,})\.?){2,3})(.*)/\1VERSION: ${NEW_VERSION}\5/g" ${THIS_DIR}/README.md

spec_update:  ## Update Generated files
	generate-spec-docs -p ${THIS_DIR}/specker/ -o ${THIS_DIR}/SPEC.md.tmp
	sed -r 's/(Auto generated from .spec files)\n?$$/\1\n\n---TOPBLOCK---\n/g' SPEC.md.tmp > SPEC.md.tmp1
	mv SPEC.md.tmp1 SPEC.md.tmp
	sed -r 's/^(## Spec for argmap.root)/---SNIP---\n\1/g' SPEC.md.tmp | sed -r 's/(## Spec for argmap.command.args)\n?$$/---END SNIP---\n\1/g' > SPEC.md.tmp1
	mv SPEC.md.tmp1 SPEC.md.tmp
	sed -Ez 's/(.*|\n)---TOPBLOCK---\n(.*|\n)---SNIP---\n(.*|\n)\n---END SNIP---\n(.*|\n)/\1\3\2\4/g' SPEC.md.tmp > SPEC.md.tmp1
	mv SPEC.md.tmp1 SPEC.md.tmp
	sed -r 's/Spec for argmap.root/Top Level/g' SPEC.md.tmp > SPEC.md.tmp1
	mv SPEC.md.tmp1 SPEC.md.tmp

	mv SPEC.md.tmp SPEC.md
	pip install -r ${THIS_DIR}/util/requirements.txt --upgrade
	${THIS_DIR}/util/generate-specifications.py

spec_lint:  ## Specification Linting
	@echo "Validating Specker JSON"
	@(ls ${THIS_DIR}/specker/*.spec | while read f; do echo $$f; jq '.' $$f >/dev/null; done && echo " - Valid") || (echo " - FAILED" && exit 1)
	@(test $$(${THIS_DIR}/util/whitespace-scan.sh) -eq 0 && echo " - OK") || (echo " - FAILED" && exit 1)
