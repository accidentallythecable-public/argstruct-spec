#!/usr/bin/env python3
# Copyright 2023-2024 by AccidentallyTheCable <cableninja@cableninja.net>.
# All rights reserved.
# This file is part of The ArgStruct Specification,
# and is released under "GPLv3". Please see the LICENSE
# file that should have been included as part of this package.
#### END COPYRIGHT BLOCK ###
import os
from pathlib import Path
import typing
import json
from sys import exit as sys_exit

from atckit.utilfuncs import UtilFuncs
from specker.loader import SpecLoader,SpecContent

version:typing.Union[str,None] = os.environ.get("PROJECT_VERSION")
if version is None:
    print("ERROR: Must specify `PROJECT_VERSION` env variable")
    sys_exit(1)

root_dir:Path = Path(__file__).parent.parent.resolve()
specker = SpecLoader(root_dir.joinpath("specker").resolve())

def spec_dump(spec:SpecContent) -> dict[str,typing.Any]:
    """SpecContent parser
    @param SpecContent \c spec SpecContent Object
    @retval dict[str,Any] Spec Content as a Dictionary, including all spec_chains within it
    """
    attrs:list[str] = [ "name", "required", "default", "type", "comment", "example", "values", "format", "spec_chain" ]
    out:dict[str,typing.Any] = {}
    for attr in attrs:
        v:typing.Any = spec.get(attr)
        if v is not None and v != "":
            if isinstance(v,list) and len(v) == 0:
                continue
            out[attr] = v
    spec_chain:typing.Union[str,None] = spec.get("spec_chain")
    if spec_chain is not None and len(spec_chain) > 0:
        out["chain"] = {}
        for name,chain in specker.get(spec_chain).items():
            out["chain"][name] = spec_dump(chain)
    return out

if __name__ == "__main__":
    print(f"Generating ArgStruct Specifications for Version {version}")
    d:dict[str,typing.Any] = {}
    d["specification"] = {
        "version": version,
        "documentation": f"https://gitlab.com/accidentallythecable-public/argstruct-spec/-/blob/tags/versions/{version}/README.md"
    }
    top_spec:dict[str,SpecContent] = specker.get("argmap.root")
    for s,b in top_spec.items():
        d[s] = spec_dump(b)

    for format in [ "json", "yaml", "toml" ]:
        with open(root_dir.joinpath(f"specifications/argstruct-{version}.{format}"), "w", encoding="utf-8") as f:
            if format == "json":
                a:dict[str,typing.Any] = { "indent": 4 }
                f.write(json.dumps(d,indent=4))
            else:
                f.write(UtilFuncs.dump_sstr(d,format))
