#!/usr/bin/env bash
# Copyright 2023-2024 by AccidentallyTheCable <cableninja@cableninja.net>.
# All rights reserved.
# This file is part of The ArgStruct Specification,
# and is released under "GPLv3". Please see the LICENSE
# file that should have been included as part of this package.
#### END COPYRIGHT BLOCK ###

source $(dirname ${0})/../automation/scripts/whitespace.source.sh

result=$(scan_trailing_spaces md)
echo $result
