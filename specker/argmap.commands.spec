{
    "__any_item__": {
        "required": false,
        "type": "dict",
        "spec_chain": "argmap.command",
        "comment": "Command Data",
        "example": {
            "auth_required": true,
            "api_hidden": false
        }
    }
}
