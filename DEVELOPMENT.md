
# Development

- [Development](#development)
  - [Changes](#changes)
  - [Bug Fixes](#bug-fixes)
  - [Major Changes](#major-changes)

As this Specification evolves, changes shall be made following the below information

## Changes

Any change will always:
 - Be accompanied by a version change to the `VERSION` information at the top of this document
 - Be accompanied by a new git tag under `tags/versions/Major.Minor.Patch`
 - Update the associated `tags/release/Major.Minor` git tag
 - Include a section in the [CHANGELOG.md](CHANGELOG.md). If this file is not included, it may be located in the CI/CD artifacts from the associated Merge Request of the `VERSION` in question
 - Be a `Major.Minor` Version change (ex if the current version is `1.0.12` a new change would bring `1.1.0`)

Changes are defined as:
 - Additions to the Specification
 - Removals from the Specification
 - Changes to existing Specification data, which is not considered [a bug](#bugs-fixes)

Removals will be subject to the following:
 - Removals cannot occur within 2 `Major.Patch` versions
 - Removals must be preceeded by a Deprecation notice in the specification, as well as a note in the [CHANGELOG.md] regarding the deprecation, and which version the removal should occur.

## Bug Fixes

In addition to the [changes](#changes) assurances, any bug fix will always:
 - Be a `Major.Minor.Patch` Version change (ex if the current version is `1.0.12` a new bug fix would bring `1.1.13`)

Bug Fixes are defined as:
 - Conflicting Information between this document and the actual [Specification](SPEC.md)
 - Misconfiguration of the Specification or its values
 - Other items may also fall in this category

## Major Changes

A Major change means that the entire ArgStruct structure will have changed, and will no longer be even partially compatible with a previous version. These changes should be rare.
