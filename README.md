# ArgStruct - API and CLI Argument Structure Specification

<b><i><u>VERSION: 1.1.0</u></i></b>

- [ArgStruct - API and CLI Argument Structure Specification](#argstruct---api-and-cli-argument-structure-specification)
  - [Description](#description)
  - [Libraries](#libraries)
  - [Specification Files](#specification-files)
  - [Format](#format)
  - [Structure](#structure)
    - [Commands](#commands)
      - [Example Command](#example-command)
    - [Arguments](#arguments)
        - [Example Argument](#example-argument)
      - [Argument Types](#argument-types)
      - [`any_arg` Arguments](#any_arg-arguments)
  - [Previous Versions](#previous-versions)
  - [Notes](#notes)
    - [Contributing](#contributing)
      - [Contributors / Authors](#contributors--authors)
    - [License](#license)

## Description

ArgStruct is a structured document which allows the specification of API and/or CLI commands, their arguments, and argument details, in a uniform and reusable manner. This document and the associated documents will describe the specification for ArgStruct.

ArgStruct is intended to be a structure that can be used for any process which specifies arguments through an accessible interface. It is also intended to be extendable to attempt anticipation of special use cases.

## Libraries

Listed below are the languages and lists of libraries that ArgStruct is currently implemented in. <sup><a name="note-1-ref">&nbsp;</a>[1](#note-1)</sup>

 - Python:
   - [`argstruct`](https://pypi.org/project/argstruct/) | [gitlab](https://gitlab.com/accidentallythecable-public/python-modules/python-argstruct)

If you decide to build an ArgStruct parser for another language, please [create an issue](https://gitlab.com/accidentallythecable-public/argstruct-spec/-/issues/new), or, Fork and create a Pull Request.

## Specification Files

Specifications have been generated in JSON, TOML, and YAML, located in the `specifications/` directory. Each file contains the full specification. The `spec_name` indicates that it there is an associated deeper level of the specification, which is stored under `chain`. Entries called `__any_item__` allow for any key name, but must follow the specified specification.

Because the JSON, YAML and TOML formats do not have a way to reference other blocks of the specification, there may be duplicated chains within the specifications. These can be discovered by matching up `spec_chain` values. This information is also available in the [specification document](SPEC.md)

<sup><a name="note-2-ref">&nbsp;</a>[Note](#note-2)</sup>

## Format

The ArgStruct format can be created from TOML, YAML or JSON, other formats might be possible, but these should be the primary formats. For specification documents, most examples will be shown using TOML, for easier reading.

## Structure

ArgStruct's Structure consists of two Top Level dictionary blocks, `commands` and `auth_args`.

*Additional blocks may be added, depending on purpose*

See [SPEC.md](SPEC.md) for a detailed view of the Structure

### Commands

Each Command is made up of a number of options which specify if authorization is required (`auth_required`), its `ui_label` for Documentation and UI uses, `help` information about what the command does, its visibility (via `cli_hidden` and `api_hidden`), a `group` for organization and menus, and finally, its `args`.

*Additional options may be added, depending on purpose*

#### Example Command

This would create the Command 'Command List', referred to as `command_list`

```toml
[commands.command_list] # This is the command name, `command_list`
auth_required = false # Whether or not Auth is required, and whether or not to include the `auth_args` as part of its arguments
cli_hidden = false # Hide from CLI/Console commands and help
api_hidden = false # Hide from API commands and help
help = "List of Commands" # Command Information and Help string
group = "General" # Organizational Groupings for Menus, Documentation, etc
ui_label = "Command List" # Command Name for UI, Menus, Documentation, etc
```

### Arguments

**Note: This also applies to `auth_args` as well as `args`**

`args` is a dictionary containing the argument and API variable name (its key), the argument `type`, whether or not the argument is `required`, its `default` value, acceptable `values`, `help` information, its `cli_flag_names` and how to handle the argument (`multi` and `any_arg`)

*Additional options may be added, depending on purpose*

##### Example Argument

This would add the argument `myarg` to the `command_list` Command

```toml
[commands.command_list.args.myarg] # 'myarg' becomes the variable name in API and for CLI parsing
cli_flag_names = [ "-m", "--my-arg" ] # A list of CLI flags that are associated with this argument
type = "str" # The arguments required type
required = true # Whether or not the argument is required
help = "My cool argument" # Argument Information and Help string
multi = false # Allow for using the argument multiple times
any_arg = false # Specify that this argument is actually a `--argument sub_argument=value`
values = [ "a", "b", "c" ] # Specify allowed argument values
```

#### Argument Types

The `type` of an argument is limited to the following values:

 - `any`: Accept Any Value, do not check its type
 - `str`: Only Accept Strings
 - `int`: Only Accept Integers (Strings are attempted to be converted to int)
 - `list`: Only Accept List of any values
 - `dict`: Only Accept Dictionary of any values
 - `float`: Only Accept Floating points
 - `bool`: Only Accept Boolean

Types must be limited to types which can be parsed via CLI/Console, otherwise use 'any'.

#### `any_arg` Arguments

The `any_arg` flag specifes that arguments will be nested below the flagged argument. For example, if we call `--argument ab=c de=f` with this flag enabled, in data we should expect to see the below output. These arguments must be documented separately, and are not currently covered by this specification.

```json
{
    "argument": {
        "ab": "c",
        "de": "f"
    }
}
```

## Previous Versions

 - [1.0](https://gitlab.com/accidentallythecable-public/argstruct-spec/-/tree/tags/release/1.0)

## Notes
 - <a name="note-1">&nbsp;</a>[1](#note-1-ref): As this is an early project, unless others contribute, there will likely not be additional languages supported.
 - <a name="note-2">&nbsp;</a>[2](#note-2-ref): Specification documents (including `SPEC.md`) are generated from Specker Spec files, located in the `specker/` directory

### Contributing

This Specification is open to contributions, if you wish to create features or modifications to this specification, you are free to fork and make a Pull Request.

#### Contributors / Authors

 - [AccidentallyTheCable](https://gitlab.com/AccidentallyTheCable)

### License

This Specification is GPLv3 licensed, See [LICENSE.md]
